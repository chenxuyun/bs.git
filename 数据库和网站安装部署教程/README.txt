
部署说明：
首先确保你的服务器具有mysql，Apache，php运行环境，没有的话自行安装，建议使用宝塔面板
1.上传vue到你的服务器，然后解压
2.新建数据库practice：数据表（sitedata(首页视频搜索)，login（注册登录表），comment（本站评论表），aboutme（关于页面））
3.修改dbServerConnect.php，链接你的数据库即可


文件说明：

vue: 网站程序源码


数据库：网站数据表，仅表结构,不包含数据(sitedata，login，comment，aboutme)


dbServerConnect.php：这个文件非常关键，用来连接数据库，该文件在源码php文件中，打开文件夹，链接你的数据库，修改下面这行代码即可

$link = mysqli_connect('localhost', 'practice', 'kiP4FdGsj','practice');//连接数据库，数据库地址（localhost），数据库名（practice），数据库密码（kiP4FdGsj），数据表（practice）



运行环境：mysql5.6.4，Apache2.4.4，php7.3（版本不一定要一样）



如有问题请联系本程序开发者QQ：1724940500

微信公众号：软件聚导航

访问链接查看效果：https://www.52luntan.xyz/myPractice/vue

GitHub：
https://github.com/NET666/fristproject.git

